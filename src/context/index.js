import createDataContext from './createDataContext';

const initialState = {
	fte: { value: 1 },
	activities: { value: 'beautyCare', label: 'Beauty care' },
	values: [],
	showForm: false,
};

const actionTypes = {
	addFte: 'add_fte',
	addAct: 'add_act',
	shouldShowForm: 'should_sf',
};

const reducer = (state, action) => {
	switch (action.type) {
		case actionTypes.addFte:
			return { ...state, fte: action.payload };
		case actionTypes.addAct:
			return { ...state, activities: action.payload };
		case actionTypes.shouldShowForm:
			return { ...state, showForm: !state.showForm };
		default:
			console.error('Wron case');
			break;
	}
};

const addFte = (dispatch) => {
	return (val) => dispatch({ type: actionTypes.addFte, payload: val });
};
const addAct = (dispatch) => {
	return (val) => dispatch({ type: actionTypes.addAct, payload: val });
};
const shouldShowForm = (dispatch) => {
	return () => dispatch({ type: actionTypes.shouldShowForm });
};

export const { Context, Provider } = createDataContext(reducer, { addFte, addAct, shouldShowForm }, initialState);
