import React from 'react';

import RightSide from '../RightSideComponent';
import LeftSide from '../LeftSideComponent';

import './index.scss';

function Main() {
	return (
		<main className="main-container">
			<div className="main-container__left-side">
				<LeftSide />
			</div>
			<div className="main-container__right-side">
				<RightSide />
			</div>
		</main>
	);
}

export default Main;
