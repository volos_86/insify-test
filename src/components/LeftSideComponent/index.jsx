import React, { useContext } from 'react';

import BtnBlock from '../../elements/BtnBlockElement';
import FormBlock from '../../elements/FormBlockElement';
import { Context } from '../../context';

import { FaChevronLeft } from 'react-icons/fa';

import './index.scss';

function LeftSide() {
	const {
		state: {
			fte: { value: people },
			activities: { label: activities },
			showForm,
		},
		shouldShowForm,
	} = useContext(Context);

	return (
		<div className="left-side-container">
			<div className="left-side-container__wrapper">
				<button className="left-side-container__back-btn">
					<FaChevronLeft className="left-side-container__chevron" />
					<span>back</span>
				</button>
				<div className="left-side-container__content">
					Thanks. According to the{' '}
					<a href="http://localhost:4200" title="Our CoC here!">
						CoC
					</a>{' '}
					your company size is <span>{people < 101 ? people : '100+'} fte</span> and has activities in{' '}
					<span>{activities}</span>. Is that correct?
				</div>
				<>{showForm ? <FormBlock /> : <BtnBlock noCb={shouldShowForm} />}</>
			</div>
		</div>
	);
}

export default LeftSide;
