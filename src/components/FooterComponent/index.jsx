import React from 'react';

import Chat from '../ChatComponent';

import { FaRegHeart } from 'react-icons/fa';

import './index.scss';

function Footer() {
	return (
		<footer className="footer-container">
			<div className="footer-container__chat">
				<Chat />
			</div>
			<div className="footer-container__bookmark">
				<FaRegHeart style={{ verticalAlign: 'middle' }} /> Save for latter
			</div>
		</footer>
	);
}

export default Footer;
