import React, { useState } from 'react';

import { FaCircle } from 'react-icons/fa';

import './index.scss';

function Chat() {
	const [status] = useState(true);
	return (
		<div className="chat-container">
			<div className="chat-container__icon"></div>
			<div className="chat-container__main-block">
				<div>
					<span className="chat-container__title">Chat with our experts </span>
					<FaCircle
						style={{
							fontSize: '7px',
							color: status ? 'green' : null,
							verticalAlign: 'middle',
						}}
					/>
					<span className="chat-container__status-text"> online</span>
				</div>
				<div className="chat-container__subtitle">Instant answers to your questions</div>
			</div>
		</div>
	);
}

export default Chat;
