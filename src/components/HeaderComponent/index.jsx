import React from 'react';

import './index.scss';

function Header() {
	return (
		<header className="header-container">
			<div className="header-container__logo">
				<span>insify</span>
			</div>
			<div className="header-container__info">
				Let's get <b>Amsterdam Barber Brothers</b> covered
			</div>
		</header>
	);
}

export default Header;
