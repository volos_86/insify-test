import React, { useContext } from 'react';
import Line from '../../elements/LineElement';
import { Context } from '../../context';

import './index.scss';

function RightSide() {
	const { state } = useContext(Context);
	const buildChart = () => {
		const lines = [
			{ title: 'Bodily Injury', value: 0, k: 1.3 },
			{ title: 'Property Damage', value: 0, k: 1.5 },
			{ title: 'Court and legal fees', value: 0, k: 1.05 },
			{ title: 'Company vehicles', value: 0, k: 1.33 },
			{ title: 'Injures to you or your employees', value: 0, k: 1.03 },
			{ title: 'Damage to your equipment', value: 0, k: 1.033 },
			{ title: 'Propducts you produce', value: 0, k: 1.08 },
		];
		const accK = {
			beautyCare: 1.3,
			hairdressing: 1.1,
		};

		return lines.map((item) => {
			let val = state.fte.value * accK[state.activities.value] * item.k;
			return { ...item, value: val >= 100 ? 100 : val };
		});
	};
	return (
		<div className="right-side-container">
			<div className="right-side-container__wrapper">
				<div className="right-side-container__title right-side-container__title--white">
					Covering these areas is especially important for you:
				</div>
				{buildChart().map((item) => (
					<Line key={item.title} title={item.title} value={item.value} />
				))}
			</div>
		</div>
	);
}

export default RightSide;
