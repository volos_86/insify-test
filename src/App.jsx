import React from 'react';

import Header from './components/HeaderComponent';
import Main from './components/MainComponent';
import Footer from './components/FooterComponent';

import './App.scss';

function App() {
	return (
		<div className="App">
			<Header />
			<Main />
			<Footer />
		</div>
	);
}

export default App;
