import React from 'react';

import './index.scss';

function Line({ title = 'Bodily Injury', value = 55 }) {
	return (
		<div className="line-container">
			<div className="line-container__title">{title}</div>
			<div className="line-container__wrapper">
				<div className="line-container__body" style={{ width: `${value}%` }}></div>
				<div className="line-container__shadow"></div>
			</div>
		</div>
	);
}

export default Line;
