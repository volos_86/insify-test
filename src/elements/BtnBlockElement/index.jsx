import React from 'react';

import './index.scss';

function BtnBlock(props) {
	const {
		yesTitle = 'Yes it is',
		noTitle = "No, it's not",
		yesCb = () => console.log('yes'),
		noCb = () => console.log('no'),
	} = props;
	return (
		<div className="btn-block-container">
			<button className="btn-block-container__btn-yes" onClick={() => yesCb()}>
				{yesTitle}
			</button>
			<button className="btn-block-container__btn-no" onClick={() => noCb()}>
				{noTitle}
			</button>
		</div>
	);
}

export default BtnBlock;
