import React, { useContext } from 'react';
import Select from 'react-select';
import Range from 'react-input-range';
import 'react-input-range/lib/css/index.css';

import BtnBlock from '../BtnBlockElement';

import { Context } from '../../context';

import './index.scss';

function FormBlock() {
	const {
		state: {
			fte: { value: rangeValue },
		},
		addFte,
		addAct,
		shouldShowForm,
	} = useContext(Context);

	const optionsSelect = [
		{ value: 'beautyCare', label: 'Beauty care' },
		{ value: 'hairdressing', label: 'Hairdressing' },
	];

	const customSelectStyle = {
		indicatorSeparator: (provided) => {
			return { ...provided, backgroundColor: 'transparent' };
		},
		control: (provided) => {
			return { ...provided, height: '50px' };
		},
	};

	return (
		<div className="form-block-container">
			<div className="form-block-container__form-title">What's your company size?</div>
			<div className="form-block-container__range">
				<div className="form-block-container__range-label">Just me</div>
				<Range
					maxValue={101}
					minValue={1}
					value={rangeValue}
					formatLabel={() => null}
					onChange={(value) => addFte({ value })}
				/>
				<div className="form-block-container__range-label">100+</div>
				<div className="form-block-container__range-value">{rangeValue < 101 ? rangeValue : '100+'} fte</div>
			</div>

			<div className="form-block-container__form-title">And what are your main activities?</div>
			<Select
				styles={customSelectStyle}
				options={optionsSelect}
				defaultValue={optionsSelect[0]}
				onChange={(val) => addAct(val)}
			/>
			<BtnBlock yesTitle="Continue" noTitle="Cancel" noCb={shouldShowForm} />
		</div>
	);
}

export default FormBlock;
